package cz.cvut.fel.ts1.pechnmic;

public class Faktorial{

    public long factorial(int n) {
        if(n == 0) return 1;
        else{
            return n * factorial( n-1);
        }
    }
}



